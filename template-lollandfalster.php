<?php
/**
 * Template Name: Lolland Falster
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<div class="lollandfalster-hero">

  <div class="col-md-12">
    <h1>lolland falster</h1>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p>
          Lolland Falster byder på mange forskellige unikke produkter, udvid din horisont, prøv noget nyt, prøv noget uvant.
        </p>
      </div>
    </div>
  </div>

</div>


<section class="lollandfalster_weekly-product">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Ugens produkt</h1>
        <hr class="orange-hr">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </p>
      </div>
    </div>
  </div>

  <div class="container-fluid lollandfalster_weekly-product--container hidden-xs">
    <div class="row no-gutter">
      <div class="col-md-6 col-sm-6">
        <img class="img-responsive lollandfalster__weekly-product--image" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lollandfalster__weekly-product.png" alt="" />
      </div>
      <div class="col-md-6 col-sm-6 lollandfalster__weekly-product--bg">
        <div class="lollandfalster-weekly-product--content">
          <h1>Kernegaarden æble cider</h1>
          <br>
          <h2>DKK 125,-</h2>
          <br>
          <p>
            Kernegaardens æblecider er lavet af æblesorterne Discovery og Rød Aroma. Den smager af sol og sommer, fordi den har en friskhed, som er læskende på en varm sommerdag. Den er fantastisk med et par isterninger på terrassen eller til maden.
          </p>

          <a class="btn btn-cart add-to-basket--cider">Tilføj til kurv</a>

          <!-- Modal -->
            <div class="modal fade" id="myModal--cider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title text-uppercase" id="myModalLabel">Din indkøbskurv</h2>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-md-3 vertical-align">
                        <img class="img-responsive lollandfalster__weekly-product--image" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lollandfalster__weekly-product--mobile.png" alt="" />
                      </div>

                      <div class="col-md-8 vertical-align">
                        <div class="col-md-6">
                          <h4>KERNEGAARDEN ÆBLE CIDER</h4>
                          <br>
                          <div class="col-md-3 no-padding">
                            <p>
                              Antal:
                              <br>
                              Smag:
                            </p>
                          </div>

                          <div class="col-md-8 col-md-offset-1 no-padding">
                            <p>
                              1 kasse (6 flasker)
                              <br>
                              Blandet
                            </p>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="modal__right-content">
                            <h4>DKK 125</h4>
                            <hr>
                            <p>
                              Leveringstid: 2-4 hverdage
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row modal-frame-border">
                      <div class="col-md-12">
                        <div class="modal_total--produkter">
                          <p>
                            Total - 1 produkt i  <a href="#">indkøbskurven:</a> <span>DKK 125</span>
                          </p>
                        </div>
                      </div>
                    </div>

                    <div class="row modal-frame-border">
                      <div class="col-md-12">
                        <div class="modal_total--produkter">
                          <p>
                            Levering ( POST Danmark - Vælg Selv Udleveringssted ) <span>DKK 25</span>
                          </p>
                        </div>
                      </div>
                    </div>

                    <div class="row modal-frame-border">
                      <div class="col-md-12">
                        <div class="modal_total">
                          <p>
                            <strong>Total:</strong> <span><strong>DKK 150</strong></span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div><!-- / modal body -->

                  <div class="modal-footer">
                    <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
                    <button type="button" class="btn btn-see-cart--global">Gå til betaling</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container lollandfalster_weekly-product--container--mobile visible-xs">
    <div class="row">
      <div class="col-xs-12">
        <div class="weekly-product-card">
          <img class="img-responsive lollandfalster__weekly-product--image" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lollandfalster__weekly-product--mobile.png" alt="" />
          <hr>
          <h1>Kernegaarden æble cider</h1>
          <hr>
          <h2>DKK 15</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Donec odio dui, pellentesque non semper commodo,
            venenatis ut dui.
          </p>

          <a class="btn btn-cart" data-toggle="modal" data-target="#myModal--cider-mobile">Tilføj til kurv</a>

          <div class="modal fade" id="myModal--cider-mobile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h2 class="modal-title text-uppercase" id="myModalLabel">Varen er lagt i din kurv</h2>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-3 vertical-align">
                      <img class="img-responsive lollandfalster__weekly-product--image" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lollandfalster__weekly-product--mobile.png" alt="" />
                    </div>

                    <div class="col-md-8 vertical-align">
                      <div class="col-md-6">
                        <h4>KERNEGAARDEN ÆBLE CIDER</h4>
                        <br>
                        <div class="col-md-3 no-padding">
                          <p>
                            Antal:
                            <br>
                            Smag:
                          </p>
                        </div>

                        <div class="col-md-8 col-md-offset-1 no-padding">
                          <p>
                            1 kasse (6 flasker)
                            <br>
                            Blandet
                          </p>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="modal__right-content">
                          <h4>DKK 125</h4>
                          <hr>
                          <p>
                            Leveringstid: 2-4 hverdage
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row modal-frame-border">
                    <div class="col-md-12">
                      <div class="modal_total--produkter">
                        <p>
                          Total - 1 produkt i  <a href="#">indkøbskurven:</a> <span>DKK 125</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="row modal-frame-border">
                    <div class="col-md-12">
                      <div class="modal_total--produkter">
                        <p>
                          Levering ( POST Danmark - Vælg Selv Udleveringssted ) <span>DKK 25</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="row modal-frame-border">
                    <div class="col-md-12">
                      <div class="modal_total">
                        <p>
                          <strong>Total:</strong> <span><strong>DKK 150</strong></span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div><!-- / modal body -->

                <div class="modal-footer">
                  <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
                  <button type="button" class="btn btn-see-cart--global">Gå til betaling</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="front-page_featured-products">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Nyeste produkter fra Lolland Falster</h1>
        <hr class="orange-hr">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </p>
      </div>
    </div>

      <div class="row mt col__xs--margin-bottom">
        <div class="col-md-3">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_onions.png" alt="" />
            <hr>
            <h3>Rødløg fra Læsø</h3>
            <p>
              10 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--onions"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-3">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_olivenolie.png" alt="" />
            <hr>
            <h3>Olivenolie fra Torrig</h3>
            <p>
              20 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--olivenolie"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-3">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_beef.png" alt="" />
            <hr>
            <h3>Oksekød fra Dannemare</h3>
            <p>
              50 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--beef"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-3">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_marmelade.png" alt="" />
            <hr>
            <h3>Marmelade fra Bandholm</h3>
            <p>
              40 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--marmelade"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-3">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_salad.png" alt="" />
            <hr>
            <h3>Salat fra Errindlev</h3>
            <p>
              40 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--salad"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-3">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_honey.png" alt="" />
            <hr>
            <h3>Honning fra Nysted</h3>
            <p>
              40 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--honey"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-3">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_shrooms.png" alt="" />
            <hr>
            <h3>Svampe fra Onsevig</h3>
            <p>
              40 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--shrooms"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-3">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_cheese.png" alt="" />
            <hr>
            <h3>Ost fra Nørreballe</h3>
            <p>
              40 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--cheese"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

      </div>
    </div>
</section>




<!-- Modals -->
  <div class="modal fade" id="myModal--onions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">rødløg fra Gammelgaarden på Fyn</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_onions.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Rødløg fra Fyn</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 kg
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
              <div class="modal_total--produkter">
                <div class="col-md-6">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Audeo dicere, inquit. Teneo, inquit, finem illi videri nihil dolere. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Duo Reges: constructio interrete. At ille pellit, qui permulcet sensum voluptate. Inquit, dasne adolescenti veniam? Si enim ad populum me vocas, eum. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Quod autem satis est, eo quicquid accessit, nimium est;
                  </p>
                </div>

                <div class="col-md-6">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Audeo dicere, inquit. Teneo, inquit, finem illi videri nihil dolere. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Duo Reges: constructio interrete. At ille pellit, qui permulcet sensum voluptate. Inquit, dasne adolescenti veniam? Si enim ad populum me vocas, eum. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Quod autem satis est, eo quicquid accessit, nimium est;
                  </p>
                </div>
              </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--olivenolie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">rødløg fra Gammelgaarden på Fyn</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_olivenolie.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Olivenolie fra vestfyn</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 liter
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
              <div class="modal_total--produkter">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Audeo dicere, inquit. Teneo, inquit, finem illi videri nihil dolere. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Duo Reges: constructio interrete. At ille pellit, qui permulcet sensum voluptate. Inquit, dasne adolescenti veniam? Si enim ad populum me vocas, eum. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Quod autem satis est, eo quicquid accessit, nimium est;
                  </p>
                </div>
              </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--beef" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">rødløg fra Gammelgaarden på Fyn</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_beef.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Oksekød fra Nordjylland</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    2 kg
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
              <div class="modal_total--produkter">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Audeo dicere, inquit. Teneo, inquit, finem illi videri nihil dolere. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Duo Reges: constructio interrete. At ille pellit, qui permulcet sensum voluptate. Inquit, dasne adolescenti veniam? Si enim ad populum me vocas, eum. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Quod autem satis est, eo quicquid accessit, nimium est;
                  </p>
                </div>
              </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--marmelade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">rødløg fra Gammelgaarden på Fyn</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_marmelade.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Marmelade fra lolland</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 glas (500 gram)
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
              <div class="modal_total--produkter">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Audeo dicere, inquit. Teneo, inquit, finem illi videri nihil dolere. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Duo Reges: constructio interrete. At ille pellit, qui permulcet sensum voluptate. Inquit, dasne adolescenti veniam? Si enim ad populum me vocas, eum. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Quod autem satis est, eo quicquid accessit, nimium est;
                  </p>
                </div>
              </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--salad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">Salat fra Rosengaarden på Falster</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_salad.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Salat fra falster</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 kg
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ille, ut dixi, vitiose. Et harum quidem rerum facilis est et expedita distinctio. De illis, cum volemus. Quae contraria sunt his, malane? Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Duo Reges: constructio interrete.
                  </p>
                  <br>
                  <p>
                    Mihi, inquam, qui te id ipsum rogavi? Quae quidem sapientes sequuntur duce natura tamquam videntes; Et quod est munus, quod opus sapientiae? Facile est hoc cernere in primis puerorum aetatulis. Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? Eam tum adesse, cum dolor omnis absit;
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--honey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">Honning fra Nørregaarden</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_honey.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Honning fra Nørregaarden</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 glas (500 g)
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ille, ut dixi, vitiose. Et harum quidem rerum facilis est et expedita distinctio. De illis, cum volemus. Quae contraria sunt his, malane? Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Duo Reges: constructio interrete.
                  </p>
                  <br>
                  <p>
                    Mihi, inquam, qui te id ipsum rogavi? Quae quidem sapientes sequuntur duce natura tamquam videntes; Et quod est munus, quod opus sapientiae? Facile est hoc cernere in primis puerorum aetatulis. Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? Eam tum adesse, cum dolor omnis absit;
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--shrooms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">Svampe fra Onsevig</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_shrooms.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Svampe fra Onsevig</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    500 g
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 30</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ille, ut dixi, vitiose. Et harum quidem rerum facilis est et expedita distinctio. De illis, cum volemus. Quae contraria sunt his, malane? Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Duo Reges: constructio interrete.
                  </p>
                  <br>
                  <p>
                    Mihi, inquam, qui te id ipsum rogavi? Quae quidem sapientes sequuntur duce natura tamquam videntes; Et quod est munus, quod opus sapientiae? Facile est hoc cernere in primis puerorum aetatulis. Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? Eam tum adesse, cum dolor omnis absit;
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--cheese" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">Ost fra Nørreballe</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_cheese.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Ost fra Nørreballe</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    500 g
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 30</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ille, ut dixi, vitiose. Et harum quidem rerum facilis est et expedita distinctio. De illis, cum volemus. Quae contraria sunt his, malane? Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Duo Reges: constructio interrete.
                  </p>
                  <br>
                  <p>
                    Mihi, inquam, qui te id ipsum rogavi? Quae quidem sapientes sequuntur duce natura tamquam videntes; Et quod est munus, quod opus sapientiae? Facile est hoc cernere in primis puerorum aetatulis. Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? Eam tum adesse, cum dolor omnis absit;
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">Din indkøbskurv</h2>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-3 vertical-align">
              <div class="card-1 cart--image-placeholder">
                <p>
                  Produktbillede
                </p>
              </div>
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Navn på produktet</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                    <br>
                    Smag:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 kg / 1 liter / 1 glas m.v.
                    <br>
                    Blandet
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 999</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border">
            <div class="col-md-12">
              <div class="modal_total--produkter">
                <p>
                  Total - 999 produkter i  <a href="#">indkøbskurven:</a> <span>DKK 999</span>
                </p>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border">
            <div class="col-md-12">
              <div class="modal_total--produkter">
                <p>
                  Levering ( POST Danmark - Vælg Selv Udleveringssted ) <span>DKK 999</span>
                </p>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border">
            <div class="col-md-12">
              <div class="modal_total">
                <p>
                  <strong>Total:</strong> <span><strong>DKK 999</strong></span>
                </p>
              </div>
            </div>
          </div>
        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global">Gå til betaling</button>
        </div>
      </div>
    </div>
  </div>
