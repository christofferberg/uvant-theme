<?php
/**
 * Template Name: Kontakt
 */
?>

<div class="kontakt__wrap">
  <div class="kontakt__hero">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Kontakt os</h1>
          <p>
            Har du spørgsmål til UVANT's produkter, lyst til at give ris eller ros, <br> eller noget helt tredje, kan du benytte kontaktformularen nedenfor.
          </p>
          <p>
            Vi svarer på alle henvendelser hurtigst muligt.
          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="kontakt__form">
    <div class="container">
      <div class="row no-gutter kontakt__form--card row-eq-height">
        <div class="col-md-8 kontakt__form--card-left">
          <h2>Send os en besked</h2>

          <div class="row">
            <div class="col-sm-12 contact-form">
              <form id="contact" method="post" class="form" role="form">
            <div class="row">
              <div class="col-xs-6 col-md-6 form-group">
                <input class="form-control" id="name" name="name" placeholder="Navn" type="text" required />
              </div>
              <div class="col-xs-6 col-md-6 form-group">
                <input class="form-control" id="email" name="email" placeholder="Email" type="email" required />
              </div>

              <div class="col-xs-6 col-md-6 form-group">
                <input class="form-control" id="telefon" name="telefon" placeholder="Telefon" type="tel" required />
              </div>

              <div class="col-xs-6 col-md-6 form-group">
                <input class="form-control" id="firma" name="firma" placeholder="Firma (valgfrit)" type="text" />
              </div>
            </div>

            <textarea class="form-control" id="message" name="message" placeholder="Besked" rows="5"></textarea>
            <br />
              <div class="row">
                <div class="col-xs-12 col-md-12 form-group">
                  <button class="btn btn-submit" type="submit">Send</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4 kontakt__form--card-right">
          <h2>Kontaktinformation</h2>

          <p>
            <i class="fa fa-globe fa-fw" aria-hidden="true"></i> <span>Bispegade 5 <br> 4800 Nykøbing Falster</span>
          </p>

          <p>
            <i class="fa fa-phone-square fa-fw" aria-hidden="true"></i> 46 26 35 26
          </p>

          <p>
            <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i>  kontakt@uvant.dk
          </p>
            <br>
            <br>

            <a href="#">Facebook</a>
            <a href="#">Instagram</a>

        </div>
      </div>
    </div>
  </div>
</div>


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
