/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.

// Headroom js
var header = document.querySelector("#header");
new Headroom(header, {
  tolerance: {
    down: 5,
    up: 5
  },
  offset: 205,
  classes: {
    initial: "slide",
    pinned: "slide--reset",
    unpinned: "slide--up"
  }
}).init();


// wow.js
var wow = new WOW(
  {
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       false,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
      // the callback is fired every time an animation is started
      // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
  }
);
wow.init();


$('a.smoothscroll').click(function(event) {

        event.preventDefault();
        var link = this;
        $.smoothScroll({
          scrollTarget: link.hash,
          speed: 1200
    });
  });


// hiding modal + showing cart global
$(function() {

  $('.add-to-basket').on('click', function() {
    $('#myModal--onions').modal('hide');
    $('#myModal--olivenolie').modal('hide');
    $('#myModal--beef').modal('hide');
    $('#myModal--marmelade').modal('hide');
    $('#myModal--salad').modal('hide');
    $('#myModal--honey').modal('hide');
    $('#myModal--shrooms').modal('hide');
    $('#myModal--cheese').modal('hide');

    swal({   title: "Vare er tilføjet til kurv!",  timer: 2500,   showConfirmButton: false, type: "success" });

    setTimeout(function() {
        $('#myModal--cart').modal('show');
    }, 2500);
  });

});


// hiding modal + showing cart for the weekly product
$(function() {

  $('.add-to-basket--cider').on('click', function() {
    swal({   title: "Vare er tilføjet til kurv!",  timer: 2500,   showConfirmButton: false, type: "success" });
    setTimeout(function() {
        $('#myModal--cider').modal('show');
    }, 2500);
  });

});
