<div class="forside-hero">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>uvant</h1>
        <p>
          De forskellige landsdele i Danmark byder på mange forskellige unikke produkter, udvid din horisont, prøv noget nyt, prøv noget uvant.
        </p>

        <div class="hero-btn-wrap">
          <a class="hero-btn hero-btn-1 smoothscroll" href="#regions">Vælg region</a>
          <a class="hero-btn hero-btn-2 smoothscroll" href="#page-intro">Lær mere</a>
        </div>
      </div>
    </div>
  </div>
</div>


<section class="front-page_featured-products">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <h1>Udvalgte Produkter</h1>
        <hr class="orange-hr">
        <p>
          Vi har udvalgt en række produkter som typisk er sæsonpræget, eller også har leverandør givet et godt tilbud.
        </p>
      </div>
    </div>

      <div class="row mt col__xs--margin-bottom">
        <div class="col-md-2">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_onions.png" alt="" />
            <hr>
            <h3>Rødløg fra Sønderjylland</h3>
            <p>
              10 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--onions"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-2">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_olivenolie.png" alt="" />
            <hr>
            <h3>Olivenolie fra Vestfyn</h3>
            <p>
              <span>20kr</span> 10 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--olivenolie"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-2">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_beef.png" alt="" />
            <hr>
            <h3>Oksekød fra Nordjylland</h3>
            <p>
              10 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--beef"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-2">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_marmelade.png" alt="" />
            <hr>
            <h3>Marmelade fra Lolland</h3>
            <p>
              10 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--marmelade"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>


        <div class="col-md-2">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_salad.png" alt="" />
            <hr>
            <h3>Romainesalat fra Falster</h3>
            <p>
              10 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--salad"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>

        <div class="col-md-2">
          <div class="front-page_featured-products--single">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_honey.png" alt="" />
            <hr>
            <h3>Akaciehonning fra Sydfyn</h3>
            <p>
              10 kr
            </p>
            <a class="btn btn-buy" data-toggle="modal" data-target="#myModal--honey"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
    </div>
</section>

<section class="front-page_intro hidden-xs" id="page-intro">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Lokale produkter fra Danmarks regioner</h1>
      </div>

      <div class="col-md-6">
        <h2>Du får den bedste smag i lokale produkter</h2>
        <p>
          Lokale produkter har ofte enorm stor fokus på smagen – samtidig med at produktet skal være af en høj kvalitet. Vi har samlet en lang række produkter, hvor den gode smag og kvalitet er i fokus. Alle produkterne er særligt udvalgt fra lokale gårde og andre steder, hvor der bliver kælet ekstra for at levere smagsfyldige produkter af den højeste kvalitet.
          <br><br>
          Det er kun de allerbedste lokale varer, der kommer igennem nåleøjet og er med i udvalget inden for vareområderne mejeriprodukter, grøntsager og kød.
          <br>
          Du skal vælge lokale kvalitetsprodukter, når der skal ekstra smag til i madlavningen og kvalitet i din hverdag.
        </p>
      </div>

      <div class="col-md-6">
        <h2>Støt op om de lokale gårde</h2>
        <p>
          En vigtigt pointe som UVANT også går ind for er, at støtte op omkring de hårdt arbejdende lokale gårde – bliver der ikke støttet op i ny og næ, vil de i fremtiden ikke have noget at leve af og dette vil få konsekvenser for deres produktion og til sidst dig som forbruger.
          <br><br>
          Ydermere vil du også opleve at de produkter, som du køber fra UVANT, vil være meget mere friske end hvis du købte det i dit lokale supermarked. Vi har mulighed for, at give dine oplysninger videre til en af de lokale, hvis du er i nærheden og mangler 1 kg gulerødder for eksempel.
          <br><br>
          Ved blot en simpel opringning vil den lokale kunne gå ud i sin have og trække gulerødderne op og have dem klar til dig på 10 minutter. Mere friskt bliver det ikke!
        </p>
      </div>
    </div>
  </div>
</section>


<div class="boxes_wrap">
  <div class="container-fluid">
    <div class="row no-gutter">

      <div class="col-md-4">
        <div class="box-light">
          <div class="box-content">
            <img class="img-responsive svg-logo" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/farmer.svg" alt="" />
            <h1>Opdag</h1>
            <p>
              Opdag helt nye kvalitetsprodukter fra lokale – hvor der er gjort ekstra meget ud af den gode smag.
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-4 box-light--border">
        <div class="box-light">
          <div class="box-content">
            <img class="img-responsive svg-logo" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/click-to-buy.svg" alt="" />
            <h1>Køb</h1>
            <p>
              Støt op om de lokale og samtidig få en ekstra god smagsoplevelse med deres kvalitetsprodukter.
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="box-light">
          <div class="box-content">
            <img class="img-responsive svg-logo" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/farm-products.svg" alt="" />
            <h1>nyd</h1>
            <p>
              Forkæl dig selv og nyd en eller flere af de lokale varer inden for mejeriprodukter, grøntsager eller kød.
            </p>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<section class="front-page_regions">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="regions">
        <h1>Se produkter fra udvalgt region</h1>
        <hr class="orange-hr">
        <p>
          De forskellige regioner i Danmark, er et overflødighedshorn af kvalitetsprodukter. <br> Gå på opdagelse her.
        </p>
      </div>
    </div>
  </div>

  <div class="container-fluid front-page_regions--boxes small-gutter">
      <div class="col-lg-4 col-md-6 col-xs-12">
        <a href="#">
          <div class="front-page_regions--box-single box_hovedstaden">
            <div class="box-content_regions">
              <div class="box-content_regions--h1-orange">
                <h1>Hovedstaden</h1>
              </div>
            </div>
          </div>
        </a>
      </div>

      <div class="col-lg-4 col-md-6 col-xs-12">
        <a href="#">
          <div class="front-page_regions--box-single box_midtjylland">
            <div class="box-content_regions">
              <div class="box-content_regions--h1-dark">
                <h1>Midtjylland</h1>
              </div>
            </div>
          </div>
        </a>
      </div>

      <hr class="front-page_regions--spacer visible-md visible-sm">

      <div class="col-lg-4 col-md-6 col-xs-12">
        <a href="#">
          <div class="front-page_regions--box-single box_nordjylland">
            <div class="box-content_regions">
              <div class="box-content_regions--h1-orange">
                <h1>Nordjylland</h1>
              </div>
            </div>
          </div>
        </a>
      </div>

      <hr class="front-page_regions--spacer visible-lg">

      <div class="col-lg-4 col-md-6 col-xs-12">
        <a href="uvant/lolland-falster/">
          <div class="front-page_regions--box-single box_lollandfalster">
            <div class="box-content_regions">
              <div class="box-content_regions--h1-dark">
                <h1>Lolland Falster</h1>
              </div>
            </div>
          </div>
        </a>
      </div>

      <hr class="front-page_regions--spacer visible-md visible-sm">

      <div class="col-lg-4 col-md-6 col-xs-12">
        <a href="#">
          <div class="front-page_regions--box-single box_sjaelland">
            <div class="box-content_regions">
              <div class="box-content_regions--h1-orange">
                <h1>Sjælland</h1>
              </div>
            </div>
          </div>
        </a>
      </div>

      <div class="col-lg-4 col-md-6 col-xs-12">
        <a href="#">
          <div class="front-page_regions--box-single box_syddanmark">
            <div class="box-content_regions">
              <div class="box-content_regions--h1-dark">
                <h1>Syddanmark</h1>
              </div>
            </div>
          </div>
        </a>
      </div>
  </div>
</section>


<!-- Modals -->
  <div class="modal fade" id="myModal--onions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">rødløg fra Gammelgaarden på Fyn</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_onions.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Rødløg fra Fyn</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 kg
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Disse rødløg fra Gammelgaarden på Fyns land, har en forholdsvis mild smag, der er god i salater og som pynt på smørrebrød og i sandwich. Du kan også spise dem kogt, sauteret, stegt eller bagt.
                  </p>
                  <br>
                  <p>
                    Skallen er bordeauxfarvet, mens selve løget er bordeauxfarvet og hvidt. Rødløg hører til i gruppen af kepaløg.
                  </p>
                  <br>
                  <p>
                    Du kan få disse nye, danske rødløg fra juni-september.
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--salad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">Salat fra Rosengaarden på Falster</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_salad.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Salat fra falster</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 kg
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ille, ut dixi, vitiose. Et harum quidem rerum facilis est et expedita distinctio. De illis, cum volemus. Quae contraria sunt his, malane? Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Duo Reges: constructio interrete.
                  </p>
                  <br>
                  <p>
                    Mihi, inquam, qui te id ipsum rogavi? Quae quidem sapientes sequuntur duce natura tamquam videntes; Et quod est munus, quod opus sapientiae? Facile est hoc cernere in primis puerorum aetatulis. Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? Eam tum adesse, cum dolor omnis absit;
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--honey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">Honning fra Nørregaarden</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_honey.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Honning fra Nørregaarden</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 glas (500 g)
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ille, ut dixi, vitiose. Et harum quidem rerum facilis est et expedita distinctio. De illis, cum volemus. Quae contraria sunt his, malane? Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Duo Reges: constructio interrete.
                  </p>
                  <br>
                  <p>
                    Mihi, inquam, qui te id ipsum rogavi? Quae quidem sapientes sequuntur duce natura tamquam videntes; Et quod est munus, quod opus sapientiae? Facile est hoc cernere in primis puerorum aetatulis. Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? Eam tum adesse, cum dolor omnis absit;
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--olivenolie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">rødløg fra Gammelgaarden på Fyn</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_olivenolie.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Olivenolie fra vestfyn</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 liter
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Audeo dicere, inquit. Teneo, inquit, finem illi videri nihil dolere. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Duo Reges: constructio interrete. At ille pellit, qui permulcet sensum voluptate. Inquit, dasne adolescenti veniam? Si enim ad populum me vocas, eum. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Quod autem satis est, eo quicquid accessit, nimium est;
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--beef" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">rødløg fra Gammelgaarden på Fyn</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_beef.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Oksekød fra Nordjylland</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    2 kg
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Audeo dicere, inquit. Teneo, inquit, finem illi videri nihil dolere. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Duo Reges: constructio interrete. At ille pellit, qui permulcet sensum voluptate. Inquit, dasne adolescenti veniam? Si enim ad populum me vocas, eum. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Quod autem satis est, eo quicquid accessit, nimium est;
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--marmelade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">rødløg fra Gammelgaarden på Fyn</h2>
        </div>
        <div class="modal-body">
          <div class="row modal--row-wrap">
            <div class="col-md-3 vertical-align">
              <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/featured-products_marmelade.png" alt="" />
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Marmelade fra lolland</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 glas (500 gram)
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 10</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border modal--row-wrap">
            <div class="col-md-12">
                <div class="col-md-12">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Audeo dicere, inquit. Teneo, inquit, finem illi videri nihil dolere. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Duo Reges: constructio interrete. At ille pellit, qui permulcet sensum voluptate. Inquit, dasne adolescenti veniam? Si enim ad populum me vocas, eum. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Quod autem satis est, eo quicquid accessit, nimium est;
                  </p>
                </div>
            </div>
          </div>


        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global add-to-basket">Tilføj til kurv</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal--cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title text-uppercase" id="myModalLabel">Din indkøbskurv</h2>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-3 vertical-align">
              <div class="card-1 cart--image-placeholder">
                <p>
                  Produktbillede
                </p>
              </div>
            </div>

            <div class="col-md-8 vertical-align">
              <div class="col-md-6">
                <h4>Navn på produktet</h4>
                <br>
                <div class="col-md-3 no-padding">
                  <p>
                    Antal:
                    <br>
                    Smag:
                  </p>
                </div>

                <div class="col-md-8 col-md-offset-1 no-padding">
                  <p>
                    1 kg / 1 liter / 1 glas m.v.
                    <br>
                    Blandet
                  </p>
                </div>
              </div>

              <div class="col-md-6">
                <div class="modal__right-content">
                  <h4>DKK 999</h4>
                  <hr>
                  <p>
                    Leveringstid: 2-4 hverdage
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border">
            <div class="col-md-12">
              <div class="modal_total--produkter">
                <p>
                  Total - 999 produkter i  <a href="#">indkøbskurven:</a> <span>DKK 999</span>
                </p>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border">
            <div class="col-md-12">
              <div class="modal_total--produkter">
                <p>
                  Levering ( POST Danmark - Vælg Selv Udleveringssted ) <span>DKK 999</span>
                </p>
              </div>
            </div>
          </div>

          <div class="row modal-frame-border">
            <div class="col-md-12">
              <div class="modal_total">
                <p>
                  <strong>Total:</strong> <span><strong>DKK 999</strong></span>
                </p>
              </div>
            </div>
          </div>
        </div><!-- / modal body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-cart--global" data-dismiss="modal">Shop videre</button>
          <button type="button" class="btn btn-see-cart--global">Gå til betaling</button>
        </div>
      </div>
    </div>
  </div>


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
