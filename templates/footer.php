<footer>
  <div class="content-info">
    <div class="container">
      <div class="row">
        <div class="col-md-3 footer-col">
          <h3>Kontakt</h3>
          <p>
            UVANT
          </p>
          <p>
            Bispegade 5
            <br>
            4800 Nykøbing Falster
          </p>
          <br>
          <p>
            Mail: kontakt@uvant.dk
            <br>
            Telefon: <span class="orange-color">46 26 35 26</span>
            <br>
            CVR: <span class="orange-color">32 24 63 61</span>
          </p>
        </div>

        <div class="col-md-3 footer-col">
          <h3>Vælg region</h3>

          <ul>
            <li><a href="hovedstaden">Hovedstaden</a></li>
            <li><a href="midtjylland">Midtjylland</a></li>
            <li><a href="nordjylland">Nordjylland</a></li>
            <li><a href="lolland-falster">Lolland Falster</a></li>
            <li><a href="sjaelland">Sjælland</a></li>
            <li><a href="syddanmark">Syddanmark</a></li>
          </ul>
        </div>

        <div class="col-md-3 footer-col">
          <h3>Se også</h3>

          <ul>
            <li><a href="uvant">Forside</a></li>
            <li><a href="om-uvant">Om UVANT</a></li>
            <li><a href="kontakt">Kontakt</a></li>
            <li><a href="#">Ofte stillede spørgsmål</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p>
            Copyright &copy; 2016 UVANT All Rights Reserved.

            <a href="#"><i class="fa fa-instagram hidden-xs" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-facebook-official hidden-xs" aria-hidden="true"></i></a>
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>
